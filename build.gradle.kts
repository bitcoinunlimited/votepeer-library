import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

buildscript {
    val kotlinVersion by extra("1.6.0")
    val navigationVersion by extra("2.3.5")

    repositories {
        google()
        maven {
            setUrl("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
        classpath("com.android.tools.build", "gradle", "7.0.2")

        classpath("org.jetbrains.kotlin", "kotlin-gradle-plugin", kotlinVersion)
        classpath("org.jetbrains.kotlin", "kotlin-serialization", kotlinVersion)

        classpath("com.google.gms", "google-services", "4.3.10")
        classpath("androidx.navigation", "navigation-safe-args-gradle-plugin", navigationVersion)

        // Test coverage
        val jacoco = "0.8.5"
        classpath("org.jacoco:org.jacoco.core:$jacoco")
    }
}

plugins {
    jacoco
}

allprojects {
    repositories {
        google()
        mavenCentral()
        maven {
            setUrl("https://bitcoinunlimited.gitlab.io/libbitcoincashkotlin/repos/")
        }
    }
}
