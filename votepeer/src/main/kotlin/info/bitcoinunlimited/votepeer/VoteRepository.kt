package info.bitcoinunlimited.votepeer

import bitcoinunlimited.libbitcoincash.ChainSelector
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionService
import info.bitcoinunlimited.votepeer.tally.Tally
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.votepeer.vote.Vote
import info.bitcoinunlimited.votepeer.vote.VoteOption
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
abstract class VoteRepository @DelicateCoroutinesApi constructor(
    protected open val identityRepository: IdentityRepository,
    protected open val electionService: ElectionService,
    protected open val electrum: ElectrumAPI,
    protected open val chain: ChainSelector = Constants.CURRENT_BLOCKCHAIN
) {
    abstract val election: Election

    abstract suspend fun getVoteStringHuman(): String
    abstract suspend fun getVoteOption(): VoteOption
    abstract suspend fun getVoteTxId(): String
    abstract suspend fun vote(
        option: VoteOption
    ): Vote
    abstract suspend fun getTally(publicKeyHash: ByteArray): Tally
}