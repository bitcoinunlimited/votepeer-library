package info.bitcoinunlimited.votepeer.votePeerActivity

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.*
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.auth.AuthState
import info.bitcoinunlimited.votepeer.election.detail.ElectionDetailFragmentArgs
import info.bitcoinunlimited.votepeer.election.network.ElectionProviderRest
import info.bitcoinunlimited.votepeer.utils.onEachEvent
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class VotePeerActivityViewModel(
    private val authRepository: AuthRepository,
    private val electrumAPI: ElectrumAPI,
    private val chain: ChainSelector,
    application: Application
) : AndroidViewModel(application) {
    private val _state = MutableStateFlow<VotePeerActivityViewState?>(null)
    val state = MutableStateFlow<VotePeerActivityViewState?>(null)
    private val handler = CoroutineExceptionHandler { _, throwable ->
        val exception = Exception(throwable)
        state.value = VotePeerActivityViewState.VotePeerActivityError(exception)
    }

    fun observeAuth() {
        authRepository.authState.onEach {
            when (it) {
                is AuthState.AuthError -> {
                    val exception = it.exception
                    state.value = VotePeerActivityViewState.VotePeerActivityError(exception)
                }
                else -> {}
            }
        }.flowOn(Dispatchers.IO + handler).launchIn(viewModelScope)
    }

    fun initBlockchainConnection() = viewModelScope.launch(Dispatchers.IO + handler) {
        electrumAPI.connect()
        observeElectrumApiExceptions()
    }

    fun bindIntents(view: VotePeerActivityView) {
        view.initState().onEach {
            state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope + handler)

        view.submitConnectionStatus().onEachEvent { connection ->
            setConnectionStatus(connection)
        }.launchIn(viewModelScope + handler)
    }

    internal fun observeElectrumApiExceptions() {
        electrumAPI.exception.onEach {
            if (it != null) {
                state.value = VotePeerActivityViewState.VotePeerActivityError(it)
            }
        }.flowOn(Dispatchers.IO + handler).launchIn(viewModelScope + handler)
    }

    @Suppress("unused")
    fun startQrScanner() {
        state.value = VotePeerActivityViewState.StartQrScanner
        state.value = VotePeerActivityViewState.DefaultNone
    }

    @Suppress("unused")
    fun qrCodeRead(rawUri: String) = viewModelScope.launch(Dispatchers.IO + handler) {
        val qrData = QrCodeData(rawUri)
        authRepository.signInToWebsite(qrData)
    }

    internal fun initAuth() = viewModelScope.launch(Dispatchers.IO + handler) {
        observeAuth()
        authRepository.signInWithKeyPair()
        authRepository.updateNotificationToken()
        authRepository.linkPublicKey(chain)
    }

    private fun setConnectionStatus(
        connection: VotePeerActivityViewState.ConnectionStatus
    ) = viewModelScope.launch(Dispatchers.Main + handler) {
        state.value = connection
    }

    @Suppress("unused")
    fun handleVotePeerNotification(
        privateKeyHex: String,
        electionId: String
    ) = viewModelScope.launch(Dispatchers.IO + handler) {
        if (authRepository.authState.value is AuthState.AuthenticatedKeyPair) {
            val election = ElectionProviderRest.getElection(electionId)
            val electionTitle = election.getTitle()
            val latestBlockHeight = electrumAPI.getLatestBlockHeight() ?: 0L

            viewModelScope.launch(Dispatchers.Main + handler) {
                val electionDetailFragmentArgs = ElectionDetailFragmentArgs(electionTitle, election, latestBlockHeight, privateKeyHex)
                _state.value = VotePeerActivityViewState.Notification(electionDetailFragmentArgs)
                _state.value = VotePeerActivityViewState.DefaultNone
            }
        } else {
            return@launch
        }
    }
}
