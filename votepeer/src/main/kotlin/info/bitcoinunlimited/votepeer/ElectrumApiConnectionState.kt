package info.bitcoinunlimited.votepeer

sealed class ElectrumApiConnectionState {
    data class Connecting(
        val host: String,
        val port: Int
    ) : ElectrumApiConnectionState()

    data class Connected(
        val host: String?,
        val port: Int?
    ) : ElectrumApiConnectionState()

    data class Disconnected(
        val message: String
    ) : ElectrumApiConnectionState()

    data class ElectrumApiError(
        val exception: Exception
    ) : ElectrumApiConnectionState()
}
