package info.bitcoinunlimited.votepeer.ringSignatureVote

import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import bitcoinunlimited.libbitcoincash.*
import bitcoinunlimited.libbitcoincash.vote.RingSignatureVote
import bitcoinunlimited.libbitcoincash.vote.RingSignatureVoteBase
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.VoteRepository
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionService
import info.bitcoinunlimited.votepeer.election.RingSignatureVoteElection
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDao
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteRoom
import info.bitcoinunlimited.votepeer.tally.Tally
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.votepeer.utils.TAG_RING_SIGNATURE_VOTE_TALLYER
import info.bitcoinunlimited.votepeer.vote.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

class RingSignatureVoteNotFoundInRoomException(message: String?) : Exception(message)

/**
 * Holds the information needed to deploy a vote payload.
 */
class PayloadContractUtxo(
    val payload: ByteArray,
    val fundingUtxo: BCHspendable,
) {

    fun createInput(chain: ChainSelector, contractPrivateKey: ByteArray, contractPublicKey: ByteArray): Pair<BCHinput, ByteArray> {
        var input = BCHinput(Constants.CURRENT_BLOCKCHAIN)

        val payloadHash = PayloadContract.calcPayloadHash(contractPublicKey, payload)
        input.spendable.priorOutScript = PayloadContract.redeemScript(chain, contractPublicKey, payloadHash)
        input.spendable.amount = fundingUtxo.amount
        input.spendable.outpoint = fundingUtxo.outpoint
        input.spendable.secret = UnsecuredSecret(contractPrivateKey)
        return Pair(input, payload)
    }
}

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
@InternalCoroutinesApi
class RingSignatureVoteRepository(
    override val election: RingSignatureVoteElection,
    electionService: ElectionService,
    electrum: ElectrumAPI,
    private val ringSignatureDao: RingSignatureVoteDao,
    identityRepository: IdentityRepository,
) : VoteRepository(identityRepository, electionService, electrum) {
    override suspend fun getVoteStringHuman(): String {
        val option = ringSignatureDao.getSelectedOption(election.id)

        option?.let {
            val optionHash = election.getOptionHash(option)
            return electionService.parseVoteSentence(election, optionHash)
        }
        return "Vote: No data"
    }

    override suspend fun getVoteOption(): VoteOption {
        val optionIndex = ringSignatureDao.getSelectedOption(electionId = election.id)
        if (optionIndex === null) {
            throw RingSignatureVoteNotFoundInRoomException(getNotFoundMessage(election))
        }
        return VoteOption(optionIndex)
    }

    override suspend fun getVoteTxId(): String {
        val ringSignatureVote = ringSignatureDao.get(election.id) ?: throw Exception("Cannot get ringSignatureVoteRoom")
        return ringSignatureVote.transactionId
    }

    /**
     * fundedPayloads = List<Triple<Payload, Payload contract address, Transaction that funded address>>
     */
    suspend fun voteFundedContract(voteOption: VoteOption, fundedPayloads: Array<PayloadContractUtxo>) {
        val ringSignatureVote = RingSignatureVote(
            election.salt.toByteArray(),
            election.description,
            election.beginHeight.toInt(),
            election.endHeight.toInt(),
            election.options,
            election.participants
        )
        val (contractPrivateKey, contractPublicKey) = ringSignatureVote.createDeterministicKeypair(
            identityRepository.currentUser.secret?.getSecret()!!
        )
        val chain = Constants.CURRENT_BLOCKCHAIN
        val inputsWithVotePayload: Array<Pair<BCHinput, ByteArray>> = fundedPayloads.map {
            it.createInput(chain, contractPrivateKey, contractPublicKey)
        }.toTypedArray()

        val voteTx = ringSignatureVote.castVoteTransaction(chain, inputsWithVotePayload, true)
        electrum.broadcast(voteTx)

        // TODO: Why do we want to store the funding transactions??
        val dummyFundingTx = BCHtransaction(chain)
        insertIntoDao(voteOption, voteTx, dummyFundingTx)
    }

    override suspend fun vote(option: VoteOption): Vote {
        val optionHash = election.getOptionHash(option.selected)
        val ringSignatureVote = RingSignatureVote(
            election.salt.toByteArray(),
            election.description,
            election.beginHeight.toInt(),
            election.endHeight.toInt(),
            election.options,
            election.participants
        )

        // Fund the payload contract.
        // TODO: The funding UTXO should NOT be traceable to userCastingVote
        val utxos = electrum.listUnspent(identityRepository.currentUser)
        var fundingRequired = ringSignatureVote.calculateFundingAmountRequired(numberOfInputs = 1)

        var inputAmount = 0
        val fundingInputs = mutableListOf<BCHinput>()
        for (coin in utxos) {
            val input = BCHinput(chain)
            inputAmount += coin.amount.toInt()
            input.spendable = coin
            fundingInputs.add(input)

            if (inputAmount >= fundingRequired) {
                // Enough coins
                break
            }
            // We need another input. We must also fund its fee.
            fundingRequired += NetworkConstants.P2PKH_SCHNORR_INPUT_SIZE
        }
        if (inputAmount < fundingRequired) {
            throw Error("Not enough funds to fund ring signature contract.")
        }

        val secret = identityRepository.currentUser.secret?.getSecret()!!
        val (privateKey, _) = ringSignatureVote.createDeterministicKeypair(secret)
        val payload = ringSignatureVote.createVotePayload(optionHash, secret)
        val (fundingTx, payloadContractInputs) = ringSignatureVote.fundPayloadContract(
            chain, fundingInputs.toTypedArray(), privateKey, payload, identityRepository.currentUser.address!!
        )
        val voteTx = ringSignatureVote.castVoteTransaction(chain, payloadContractInputs, false)
        electrum.broadcast(fundingTx)
        electrum.broadcast(voteTx)

        return insertIntoDao(option, voteTx, fundingTx)
    }

    private suspend fun insertIntoDao(
        option: VoteOption,
        voteTx: BCHtransaction,
        fundingTx: BCHtransaction
    ): Vote {
        val vote = Vote(
            option = option,
            transaction = VoteTransaction.RingSignature(voteTx, fundingTx)
        )
        val ringSignatureVoteRoom = RingSignatureVoteRoom(election, vote)
        ringSignatureDao.insert(ringSignatureVoteRoom)
        return vote
    }

    private fun sortVoteTransactions(transactions: List<Pair<Long, BCHtransaction>>): List<Pair<Long, BCHtransaction>> {
        return transactions.sortedWith { a, b ->
            // If transaction is in mempool, it's height is set to 0 or negative.
            // These should be sorted at last.
            val aHeight: Long = if (a.first <= 0) a.first else Long.MAX_VALUE
            val bHeight: Long = if (b.first <= 0) b.first else Long.MAX_VALUE

            if (aHeight == bHeight) {
                // Heights are the same, order by transaction ID
                a.second.hash.compareTo(b.second.hash)
            } else {
                aHeight.compareTo(bHeight)
            }
        }
    }

    override suspend fun getTally(publicKeyHash: ByteArray): Tally {
        // Vote casting transaction have a specific output destination that we can query to find all votes.
        val ringSignatureVote = RingSignatureVote(
            election.salt.toByteArray(),
            election.description,
            election.beginHeight.toInt(),
            election.endHeight.toInt(),
            election.options,
            election.participants
        )
        val address = ringSignatureVote.getNotificationAddress(chain)

        val votes: MutableMap<String, MutableList<Pair<String?, String?>>> = mutableMapOf()

        // Initiate tally
        for (option in election.options) {
            votes[option] = mutableListOf()
        }
        votes[VoteOption.NOT_VOTED_MAGIC_INDEX] = mutableListOf()

        val acceptedVotes: MutableList<AcceptedVote> = mutableListOf()

        val maybeVoteTxs = electrum.getFundingTransactions(address, election.beginHeight, election.endHeight)

        RingSignatureVoteValidator(election.getContractElectionID(), election.participants).use { validator ->

            for ((_, tx) in sortVoteTransactions(maybeVoteTxs)) {
                try {
                    val (vote, signature) = RingSignatureVoteBase.parseVote(chain, tx)
                    if (validator.isAcceptable(vote, signature, acceptedVotes)) {
                        acceptedVotes.add(
                            AcceptedVote(
                                vote = vote,
                                signature = signature,
                                voteOption = electionService.getVoteOption(election, vote),
                                tx = tx
                            )
                        )
                    } else {
                        Log.i(TAG_RING_SIGNATURE_VOTE_TALLYER, "Vote in tx ${tx.hash.toHex()} rejected by validator")
                    }
                } catch (e: IllegalArgumentException) {
                    Log.i(TAG_RING_SIGNATURE_VOTE_TALLYER, "Ignoring not-a-vote tx ${tx.hash.toHex()}: $e")
                }
            }
        }

        for (vote in acceptedVotes) {
            val owner: String? = vote.exposedOwner?.let {
                UtilStringEncoding.toHexString(it)
            }
            votes[vote.voteOption.selected]!!.add(Pair(vote.tx.hash.toHex(), owner))
        }

        return Tally(election.id, votes)
    }

    val vote = RingSignatureVote(
        election.salt.toByteArray(),
        election.description,
        election.beginHeight.toInt(),
        election.endHeight.toInt(),
        election.options,
        election.participants
    )

    fun getPayloadContractAddresses(option: VoteOption): List<Pair<PayAddress, ByteArray>> {
        val optionHash = election.getOptionHash(option.selected)
        val secret = identityRepository.currentUser.secret?.getSecret()!!
        val (privateKey, _) = vote.createDeterministicKeypair(secret)
        val pubkey = PayDestination.GetPubKey(privateKey)
        val payload = vote.createVotePayload(optionHash, secret)
        return RingSignatureVoteBase.getPayloadContractAddresses(Constants.CURRENT_BLOCKCHAIN, pubkey, payload)
    }

    fun canBeFundedExternally(option: VoteOption): Boolean {
        return getPayloadContractAddresses(option).size == 1
    }

    fun getFundingAddress(option: VoteOption): Pair<PayAddress, ByteArray> {
        val addresses = getPayloadContractAddresses(option)
        if (addresses.size == 1) {
            return addresses.first()
        }
        // See issue: https://gitlab.com/nerdekollektivet/votepeer-library/-/issues/118
        throw Error("NYI: Elections with more than 22 participants cannot be externally funded.")
    }

    fun generateFundingQrCode(address: String, qrWidth: Int, qrHeight: Int): Bitmap {
        if (address.isEmpty()) throw IllegalArgumentException(addressEmpty)
        val writer = QRCodeWriter()
        val bitMatrix = writer.encode(address, BarcodeFormat.QR_CODE, qrWidth, qrHeight)
        val bitmap = Bitmap.createBitmap(bitMatrix.width, bitMatrix.height, Bitmap.Config.RGB_565)
        for (x in 0 until bitMatrix.width) {
            for (y in 0 until bitMatrix.height) {
                bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }
        return bitmap
    }

    companion object {
        const val addressEmpty = "Address is empty, cannot generate QR-code"
        fun getNotFoundMessage(election: Election) = "Cannot find voteOption for $election"
    }
}