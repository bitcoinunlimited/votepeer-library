package info.bitcoinunlimited.votepeer.ringSignatureVote.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import info.bitcoinunlimited.votepeer.election.RingSignatureVoteElection
import info.bitcoinunlimited.votepeer.vote.Vote
import java.io.Serializable

@Entity(tableName = "ring_signature_vote_table")
data class RingSignatureVoteRoom(
    @PrimaryKey val electionId: String,
    val selectedOption: String,
    val transactionId: String,
    val participantAddresses: Array<String>,
) : Serializable {
    constructor(election: RingSignatureVoteElection, vote: Vote) : this(
        election.id,
        vote.option.selected,
        vote.transaction.voteTxId,
        election.electionRaw.participantAddresses
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RingSignatureVoteRoom

        if (electionId != other.electionId) return false
        if (selectedOption != other.selectedOption) return false
        if (transactionId != other.transactionId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = electionId.hashCode()
        result = 31 * result + selectedOption.hashCode()
        result = 31 * result + transactionId.hashCode()
        return result
    }
}