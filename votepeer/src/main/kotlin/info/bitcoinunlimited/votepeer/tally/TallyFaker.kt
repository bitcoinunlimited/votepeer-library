package info.bitcoinunlimited.votepeer.tally

import info.bitcoinunlimited.votepeer.vote.VoteOption.Companion.NOT_VOTED_MAGIC_INDEX

object TallyFaker {
    private val options = mapOf(
        Pair("0", listOf()),
        Pair("1", listOf(Pair(null, null), Pair(null, null))),
        Pair("2", listOf(Pair(null, null), Pair(null, null), Pair(null, null))),
        Pair(
            NOT_VOTED_MAGIC_INDEX,
            listOf(
                Pair(null, null), Pair(null, null), Pair(null, null),
                Pair(null, null), Pair(null, null), Pair(null, null)
            )
        )
    )

    fun createTally(electionId: String): Tally {
        return Tally(
            electionId,
            options,
        )
    }
}