package info.bitcoinunlimited.votepeer.election.fund

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.rememberPagerState
import com.ncorti.slidetoact.SlideToActView
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.databinding.FragmentFundBinding
import info.bitcoinunlimited.votepeer.election.fund.screens.TabItem
import info.bitcoinunlimited.votepeer.election.fund.screens.Tabs
import info.bitcoinunlimited.votepeer.election.fund.screens.TabsContent
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDatabase
import info.bitcoinunlimited.votepeer.utils.InjectorUtils
import info.bitcoinunlimited.votepeer.utils.TAG_FUND_VOTE
import java.lang.Exception
import kotlinx.coroutines.*

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalPagerApi
class FundVoteFragment : Fragment(), FundVoteView {
    private val safeArgs: FundVoteFragmentArgs by navArgs()
    private var _binding: FragmentFundBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!
    private val intent = FundVoteIntent()
    @InternalCoroutinesApi
    internal val viewModel: FundVoteViewModel by viewModels {
        InjectorUtils.provideFundVoteViewModelFactory(
            safeArgs.privateKeyHex,
            safeArgs.election,
            RingSignatureVoteDatabase.getInstance(requireContext()).ringSignatureVoteDao(),
            safeArgs.voteOption,
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentFundBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.fundComposeView.apply {
            // Dispose of the Composition when the view's LifecycleOwner
            // is destroyed

            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            var lastTab = 0
            setContent {
                val contract: ContractState by viewModel.balanceState.collectAsState()
                FundScreen(
                    safeArgs.election.getTitle(),
                    viewModel.voteOptionHuman,
                    contract,
                    viewModel.fundingAddress,
                    viewModel.qrCode,
                    { copyAddressToClipBoard(viewModel.fundingAddress) },
                    { tab ->
                        if (tab != lastTab) {
                            lastTab = tab
                            viewModel.viewModelScope.launch(Dispatchers.IO + viewModel.handler) {
                                viewModel.fetchContractBalance()
                            }
                        }
                        viewModel.onTabChangeEvent(tab)
                    },
                    {
                        shareAddress(viewModel.fundingAddress)
                    },

                )
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.bindIntents(this)
        initUiListeners()
    }

    override fun onResume() {
        super.onResume()
        viewModel.subscribeAddress()
    }

    override fun onPause() {
        super.onPause()
        viewModel.unsubscribeAddress()
    }

    private fun shareAddress(address: String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, address)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    private fun initUiListeners() {
        binding.slideToVote.onSlideCompleteListener = object : SlideToActView.OnSlideCompleteListener {
            override fun onSlideComplete(view: SlideToActView) {
                viewModel.voteSliderEvent()
            }
        }
    }

    internal fun copyAddressToClipBoard(address: String) {
        val clipboardManager = activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", address)
        clipboardManager.setPrimaryClip(clipData)
        Toast.makeText(activity, "Address copied to clipboard", Toast.LENGTH_LONG).show()
    }

    override fun initExceptionState() = intent.initExceptionState
    override fun initSlideToVoteState() = intent.initSlideToVoteState
    override fun initHasVotedState() = intent.initHasVotedState

    override fun renderException(exception: Exception) {
        binding.voteFundError.text = exception.message
        binding.voteFundError.isVisible = true
        val message = "Showing error to user: $exception stacktrace: ${exception.stackTraceToString()}"
        binding.slideToVote.resetSlider()
        Log.e(TAG_FUND_VOTE, message)
    }

    override fun renderSlideToVote(display: Boolean) {
        if (display) {
            binding.slideToVote.visibility = View.VISIBLE
        } else {
            binding.slideToVote.visibility = View.GONE
        }
    }

    override fun renderHasVotedState(hasVoted: Boolean) {
        if (hasVoted)
            findNavController().popBackStack()
    }
}

@ExperimentalPagerApi
@Composable
fun FundScreen(
    electionTitle: String,
    optionHuman: String,
    contractState: ContractState,
    contractAddress: String,
    qrCode: Painter,
    copyAddress: () -> Unit,
    onTabClicked: (index: Int) -> Unit,
    shareAddress: () -> Unit,
) {
    val tabs = listOf(
        TabItem.External(electionTitle, optionHuman, contractState, contractAddress, qrCode, copyAddress, shareAddress),
        TabItem.Internal(electionTitle, optionHuman, shareAddress)
    )

    val pagerState = rememberPagerState()
    Scaffold {
        Column {
            Tabs(tabs = tabs, pagerState = pagerState, onTabClicked)
            TabsContent(tabs = tabs, pagerState = pagerState)
        }
    }
}

@ExperimentalPagerApi
@Preview(showBackground = true)
@Composable
fun FundScreenPreview() {
    val bitmap = painterResource(id = R.drawable.ic_external_black)

    FundScreen(
        "Election title!",
        "Voting for option: The rent is too damn high!",
        ContractState.LoadingBalance(cost = 0.00068, voteOptionHuman = "Voting for option: The rent is too damn high!"),
        "qpv5y82t8z7n6w80fpm64afah7ntptxue59h5cdsn2",
        bitmap,
        {},
        {}
    ) {
    }
}