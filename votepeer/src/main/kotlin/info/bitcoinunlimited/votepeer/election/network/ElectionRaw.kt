package info.bitcoinunlimited.votepeer.election.network

import kotlinx.serialization.Serializable

@Serializable
data class ElectionRaw(
    val id: String,
    val adminAddress: String,
    val description: String,
    val participantAddresses: Array<String>,
    val salt: String,
    val endHeight: Long,
    val contractType: String,
    val visibility: String,
    val options: Array<String>,
    val tags: Array<String>,
    val beginHeight: Long
) : java.io.Serializable
