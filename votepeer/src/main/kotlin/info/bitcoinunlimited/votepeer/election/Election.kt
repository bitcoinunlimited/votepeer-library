package info.bitcoinunlimited.votepeer.election

import bitcoinunlimited.libbitcoincash.UtilVote
import info.bitcoinunlimited.votepeer.election.network.ElectionRaw
import java.io.Serializable

abstract class Election(
    electionRaw: ElectionRaw,
) : Serializable {
    open val id: String = electionRaw.id
    open var voteSentence: String? = ""
    open val adminAddress: String = electionRaw.adminAddress
    open val endHeight: Long = electionRaw.endHeight
    open val options: Array<String> = electionRaw.options
    open val description: String = electionRaw.description
    open val beginHeight: Long = electionRaw.beginHeight
    open val salt: String = electionRaw.salt
    /**
     * For two-option-vote, a participant is identified by public key hash of a P2PKH address.
     * For ring signature, a participant is identified by their linked ring signature public key.
     */
    abstract val participants: Array<ByteArray>

    fun getTitle(): String {
        val maxTitleLength = 60
        val title = description.split("\n")[0]
        if (title.length > maxTitleLength) {
            return title.subSequence(0, Integer.min(title.length, maxTitleLength)).toString() + "..."
        }
        return title
    }

    /**
     * Pretty string of who hosts the election
     */
    fun getHostedByHuman(): String {
        val prefixLen = "bitcoincash:".length
        val stringBuffer = StringBuffer(adminAddress.subSequence(prefixLen, prefixLen + 10))
        stringBuffer.append("...")
        stringBuffer.append(adminAddress.subSequence(adminAddress.length - 6, adminAddress.length))
        val trimmedAddress = stringBuffer.toString()
        return "Hosted by $trimmedAddress"
    }

    fun getElectionLifecycleState(currentHeight: Long): ElectionLifecycle {
        val exception = IllegalStateException(
            "Something is wrong: beginHeight: $beginHeight, endHeight: $endHeight, currentHeight: $currentHeight"
        )
        return when {
            beginHeight >= endHeight -> throw exception
            currentHeight < beginHeight -> ElectionLifecycle.NOT_STARTED
            currentHeight in beginHeight until endHeight -> ElectionLifecycle.ONGOING
            currentHeight >= endHeight -> ElectionLifecycle.ENDED
            else -> throw exception
        }
    }

    /**
     * Get human-readable version of end time
     */
    fun getElectionLifecycleStateHuman(currentHeight: Long): String {
        return when (getElectionLifecycleState(currentHeight)) {
            ElectionLifecycle.NOT_STARTED -> {
                "⌛ Starts in: ${getTimeIntervalHuman(currentHeight, beginHeight)} at block $beginHeight"
            }
            ElectionLifecycle.ONGOING -> {
                "⚠️ Ongoing! Ends in: ${getTimeIntervalHuman(currentHeight, endHeight)} at block $endHeight"
            }
            ElectionLifecycle.ENDED -> {
                "\uD83C\uDFC1 Ended ${getTimeIntervalHuman(endHeight, currentHeight)} ago at block $endHeight"
            }
        }
    }

    internal fun getTimeIntervalHuman(startHeight: Long, endHeight: Long): String {
        if (startHeight >= endHeight) {
            throw IllegalArgumentException("startHeight: $startHeight must be less than endHeight: $endHeight")
        }
        val remainingBlocks = (endHeight - startHeight) * 600
        val minutes = remainingBlocks % 3600 / 60
        val hours = remainingBlocks % 86400 / 3600
        val days = remainingBlocks / 86400

        if (days != 0L) {
            return "$days day(s) and $hours hour(s)"
        }
        if (hours != 0L) {
            return "$hours hour(s) and $minutes minute(s)"
        }
        return "$minutes minute(s)"
    }

    fun areContentsTheSame(election: Election): Boolean {
        return (
            election.voteSentence == this.voteSentence &&
                election.adminAddress == this.adminAddress &&
                election.description == this.description &&
                election.beginHeight == this.beginHeight &&
                election.endHeight == this.endHeight &&
                election.salt == this.salt
            )
    }

    /**
     * Get hash of vote option as used in vote contracts
     */
    open fun getOptionHash(option: String): ByteArray {
        return UtilVote.hash160Salted(salt.toByteArray(), option.toByteArray())
    }
    abstract fun getContractElectionID(): ByteArray
}
