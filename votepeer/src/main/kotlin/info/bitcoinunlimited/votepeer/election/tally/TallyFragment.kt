package info.bitcoinunlimited.votepeer.election.tally

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.databinding.FragmentTallyBinding
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDatabase
import info.bitcoinunlimited.votepeer.utils.Event
import info.bitcoinunlimited.votepeer.utils.InjectorUtils
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class TallyFragment : Fragment() {
    private var _binding: FragmentTallyBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!
    private val safeArgs: TallyFragmentArgs by navArgs()
    private val intent = TallyIntent()
    private val viewModel: TallyViewModel by viewModels {
        InjectorUtils.provideElectionResultViewModelFactory(
            safeArgs.privateKeyHex,
            safeArgs.election,
            RingSignatureVoteDatabase.getInstance(requireContext()).ringSignatureVoteDao(),
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTallyBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.tallyView.apply {
            // Dispose of the Composition when the view's LifecycleOwner
            // is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val loading: Boolean by viewModel.loading.collectAsState()
                val result: Map<String, Int> by viewModel.result.collectAsState()
                val error: Exception? by viewModel.error.collectAsState()
                TallyScreen(result, safeArgs.election.id, loading, error)
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.refreshTally()
    }

    override fun onResume() {
        super.onResume()
        intent.refreshResults.value = Event(TallyIntent.RefreshResults)
    }

    @Composable
    fun BrowserButton(electionId: String) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(colorResource(id = R.color.colorWhite))
                .padding(16.dp)
        ) {
            Button(
                modifier = Modifier.padding(16.dp),
                onClick = {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse("https://voter.cash/#/election/$electionId")
                    startActivity(intent)
                },
            ) {
                Text(stringResource(id = R.string.view_in_browser))
            }
            Button(
                modifier = Modifier.padding(16.dp),
                onClick = {
                    intent.refreshResults.value = Event(TallyIntent.RefreshResults)
                },
            ) {
                Text(stringResource(id = R.string.refresh))
            }
        }
    }

    @Composable fun TallyScreen(result: Map<String, Int>, electionId: String, loading: Boolean, error: Exception?) {
        Column() {
            TallyProgressBar(loading)
            TallyError(error)
            TallyList(result)
            BrowserButton(electionId)
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun TallyScreenPreview() {
        Column() {
            val votesMock = mapOf(Pair("Yes", 0), Pair("No", 10), Pair("Maybe that", 4))
            TallyProgressBar(true)
            TallyError(Exception("Something went wrong!"))
            TallyList(votesMock)
            BrowserButton("safeArgs.election.id")
        }
    }
}

@Composable
fun TallyList(votes: Map<String, Int>) {
    Column(
        modifier = Modifier
            .background(colorResource(id = R.color.colorWhite))
            .padding(16.dp)
    ) {
        votes.forEach {
            val option = it.key
            val vote = it.value
            VoteOptionTally(option, vote)
        }
    }
}

@Composable
fun VoteOptionTally(option: String, votes: Int) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp),
        elevation = 10.dp
    ) {
        Column(
            modifier = Modifier.padding(15.dp)
        ) {
            Row() {
                Text(text = option)
                Spacer(Modifier.width(16.dp))
                Text(text = "$votes")
            }
        }
    }
}

@Composable
fun TallyProgressBar(loading: Boolean) {
    if (loading)
        LinearProgressIndicator(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .testTag("loading_tally")
        )
}

@Composable
fun TallyError(error: Exception?) {
    if (error != null) {
        Text(
            text = error.message ?: "Something went wrong!",
            fontSize = 14.sp,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .background(colorResource(id = R.color.colorAccent)),
            textAlign = TextAlign.Center
        )
    }
}
