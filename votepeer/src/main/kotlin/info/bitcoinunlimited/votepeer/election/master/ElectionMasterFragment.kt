package info.bitcoinunlimited.votepeer.election.master

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import bitcoinunlimited.libbitcoincash.*
import info.bitcoinunlimited.votepeer.ElectrumApiConnectionState
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.auth.AuthState
import info.bitcoinunlimited.votepeer.databinding.FragmentElectionMasterBinding
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.master.ElectionMasterViewState.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.utils.CONTENT_RECYCLER_VIEW_POSITION
import info.bitcoinunlimited.votepeer.utils.Event
import info.bitcoinunlimited.votepeer.utils.InjectorUtils
import kotlinx.coroutines.*

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterFragment : Fragment(), ElectionMasterView {
    private lateinit var binding: FragmentElectionMasterBinding
    private lateinit var adapter: ElectionMasterAdapter
    private var savedRecyclerPosition: Int = 0
    internal val intent = ElectionMasterViewIntent()
    internal lateinit var viewModel: ElectionMasterViewModel

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null)
            savedRecyclerPosition = savedInstanceState.getInt(CONTENT_RECYCLER_VIEW_POSITION)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val privateKeyHex = arguments?.getString("privateKeyHex") ?: throw Exception("Cannot get privateKeyHex")
        val payDestination = IdentityRepository.createPayDestination(privateKeyHex)
        super.onCreate(savedInstanceState)
        val viewModel: ElectionMasterViewModel by activityViewModels {
            InjectorUtils.provideElectionMasterViewModelFactory(
                this,
                payDestination,
                requireActivity().application
            )
        }
        this.viewModel = viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentElectionMasterBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val privateKeyHex = arguments?.getString("privateKeyHex") ?: throw Exception("Cannot get privateKeyHex")
        val payDestination: PayDestination = IdentityRepository.createPayDestination(privateKeyHex)
        initAdapter(payDestination, viewModel)
        viewModel.bindIntents(this)
        viewModel.observeElectrumApiStatus()
        viewModel.observeAuthState()
        viewModel.viewModelScope.launch(Dispatchers.IO + viewModel.handler) {
            viewModel.fetchElections()
        }
        initUiListeners()
    }

    private fun initAdapter(payDestination: PayDestination, viewModel: ElectionMasterViewModel) {
        adapter = ElectionMasterAdapter(payDestination, viewModel)
        binding.electionMasterRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.electionMasterRecyclerView.adapter = adapter
    }

    private fun renderElections(elections: List<Election>) {
        adapter.submitList(elections)
    }

    private fun initUiListeners() {
        binding.swipeToRefresh.setOnRefreshListener {
            intent.swipeToRefresh.value = Event(ElectionMasterViewIntent.SwipeToRefresh)
        }
    }

    override fun initState() = intent.initState
    override fun initElectionsState() = intent.initElectionsViewState
    override fun initElectrumApiState() = intent.initElectrumApiState
    override fun refreshSwiped() = intent.swipeToRefresh

    override fun render(state: ElectionMasterViewState) {
        when (state) {
            is LoadingSpinner -> { displaySpinningLoadingIndicator(state.loading) }
            is ErrorElectionMaster -> { renderErrorState(state) }
            is Auth -> renderAuth(state)
            else -> {}
        }
    }

    override fun renderElections(state: ElectionsViewState) {
        val feedEmpty = state.elections.isEmpty()
        displayEmptyFeedView(feedEmpty)
        binding.electionMasterRecyclerView.isVisible = !feedEmpty
        renderElections(state.elections)
    }

    override fun renderElectrumApiState(state: ElectrumApiConnectionState) {
        val electrumConnectionStatusToolbar = binding.toolbarElectrumApiConnectionStatus

        when (state) {
            is ElectrumApiConnectionState.Disconnected -> {
                val message = state.message
                electrumConnectionStatusToolbar.text = message
                electrumConnectionStatusToolbar.setBackgroundColor(resources.getColor(R.color.colorAccent))
            }
            is ElectrumApiConnectionState.Connecting -> {
                val host = state.host
                val port = state.port
                "Connecting to Electrum at: $host:$port".also { electrumConnectionStatusToolbar.text = it }
                electrumConnectionStatusToolbar.setBackgroundColor(resources.getColor(R.color.colorOrange))
            }
            is ElectrumApiConnectionState.Connected -> {
                val host = state.host
                val port = state.port

                "Connected to Electrum at: $host:$port".also { electrumConnectionStatusToolbar.text = it }
                electrumConnectionStatusToolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            }
            is ElectrumApiConnectionState.ElectrumApiError -> {
                val exception = state.exception
                "ERROR: ${exception.message}".also { electrumConnectionStatusToolbar.text = it }
                electrumConnectionStatusToolbar.setBackgroundColor(resources.getColor(R.color.colorAccent))
            }
        }
    }

    private fun renderErrorState(state: ErrorElectionMaster) {
        binding.toolbarError.isVisible = true
        binding.toolbarError.text = state.exception.message ?: state.exception.toString()
    }

    private fun displaySpinningLoadingIndicator(display: Boolean) {
        binding.swipeToRefresh.isRefreshing = display
    }

    private fun displayEmptyFeedView(display: Boolean) {
        binding.noElectionsBitcoinUnlimitedLogo.isVisible = display
        binding.noElectionsTitle.isVisible = display
        binding.noElectionsContent.isVisible = display
        binding.openVoterCash.setOnClickListener { view ->
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://voter.cash")
            startActivity(intent)
        }
        binding.openVoterCash.isVisible = display
    }

    // TODO: Write espresso-test for this render.
    private fun renderAuth(state: Auth) {
        when (val authState = state.authState) {
            is AuthState.AuthError -> renderErrorState(ErrorElectionMaster(authState.exception))
            is AuthState.AuthenticatedAnonymously -> renderAuthLoadingIndicator(true)
            is AuthState.AuthenticatedKeyPair -> renderAuthLoadingIndicator(false)
            AuthState.Unauthenticated -> renderAuthLoadingIndicator(true)
        }
    }

    private fun renderAuthLoadingIndicator(loading: Boolean) {
        if (loading) {
            binding.authenticatingProgressBar.visibility = View.VISIBLE
            binding.authenticatingText.visibility = View.VISIBLE
        } else {
            binding.authenticatingProgressBar.visibility = View.GONE
            binding.authenticatingText.visibility = View.GONE
        }
    }
}
