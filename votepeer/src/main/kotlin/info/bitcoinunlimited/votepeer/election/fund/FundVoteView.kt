package info.bitcoinunlimited.votepeer.election.fund

import info.bitcoinunlimited.votepeer.utils.Event
import java.lang.Exception
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface FundVoteView {
    fun initExceptionState(): Flow<Event<Boolean>>
    fun initSlideToVoteState(): Flow<Event<Boolean>>
    fun initHasVotedState(): Flow<Event<Boolean>>
    fun renderException(exception: Exception)
    fun renderSlideToVote(display: Boolean)
    fun renderHasVotedState(hasVoted: Boolean)
}
