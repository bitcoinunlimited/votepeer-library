package info.bitcoinunlimited.votepeer.auth

sealed class AuthState {
    internal object Unauthenticated : AuthState()

    internal data class AuthenticatedAnonymously(
        val userId: String,
    ) : AuthState()

    data class AuthenticatedKeyPair(
        val userId: String,
    ) : AuthState()

    internal data class AuthError(
        val exception: Exception
    ) : AuthState()
}
