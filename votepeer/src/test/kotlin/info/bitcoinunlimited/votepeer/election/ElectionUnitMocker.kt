package info.bitcoinunlimited.votepeer

import info.bitcoinunlimited.votepeer.election.MultiOptionVoteElection
import info.bitcoinunlimited.votepeer.election.network.ElectionRaw
import info.bitcoinunlimited.votepeer.utils.Constants
/*
* This Object runs in unit-tests and should not require dependencies that requires
* Android instrumented test-environment and a physical android device to run
 */
object ElectionUnitMocker {
    // NOTE: Do not change these without verifying that all tests run first.
    // This is basically the default test-case
    val electionRawMock = ElectionRaw(
        "mock_id",
        "bitcoincash:qp94x7qc0uh4jex8qqfghwcf37ju2nvp9q5d8rw0s0",
        "",
        arrayOf(),
        "Oh yes or oh no?",
        400L,
        Constants.MULTI_OPTION_ELECTION,
        "",
        arrayOf("Oh yes!", "Oh no!"),
        arrayOf(),
        300L
    )
    @JvmStatic
    val twoOptionElectionMock = MultiOptionVoteElection(electionRawMock, arrayOf())
}
