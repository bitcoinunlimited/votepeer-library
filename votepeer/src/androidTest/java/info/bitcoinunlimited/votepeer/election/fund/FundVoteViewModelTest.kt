package info.bitcoinunlimited.votepeer.election.fund

import bitcoinunlimited.libbitcoincash.*
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.election.ElectionFaker
import info.bitcoinunlimited.votepeer.ringSignatureVote.RingSignatureVoteRepository
import info.bitcoinunlimited.votepeer.vote.VoteOption
import info.bitcoinunlimited.votepeer.votePeerActivity.VotePeerActivityViewModelTest
import io.mockk.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
class FundVoteViewModelTest {
    private companion object {
        private val chain = ChainSelector.BCHMAINNET
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }
    private lateinit var viewModel: FundVoteViewModel
    private lateinit var electrumApi: ElectrumAPI
    private lateinit var identityRepositorySpy: IdentityRepository
    private lateinit var currentUser: Pay2PubKeyHashDestination
    private lateinit var voteRepository: RingSignatureVoteRepository
    private val testDispatcher = TestCoroutineDispatcher()

    @BeforeEach
    fun setUp() = runBlocking {
        val privateKey = ElectionFaker.mockPrivateKey('L')
        currentUser = Pay2PubKeyHashDestination(chain, UnsecuredSecret(privateKey))
        electrumApi = spyk(ElectrumAPI.getInstance(VotePeerActivityViewModelTest.chain))
        val ringSignatureVoteElection = ElectionFaker.generateOngoingRingSignatureElection("mock_election_id")
        identityRepositorySpy = spyk(IdentityRepository.getInstance(currentUser))
        voteRepository = spyk(
            RingSignatureVoteRepository(
                ringSignatureVoteElection,
                mockk(relaxed = true),
                mockk(relaxed = true),
                mockk(relaxed = true),
                mockk(relaxed = true),
            )
        )
        viewModel = FundVoteViewModel(testDispatcher, voteRepository, electrumApi, VoteOption("0"), identityRepositorySpy)
    }

    @AfterEach
    fun tearDown() = runBlocking {
        clearAllMocks()
    }
}