package info.bitcoinunlimited.votepeer.auth

import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import bitcoinunlimited.libbitcoincash.Pay2PubKeyHashDestination
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.Secret
import bitcoinunlimited.libbitcoincash.UnsecuredSecret
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.functions.FirebaseFunctions
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.election.ElectionFaker
import info.bitcoinunlimited.votepeer.utils.Constants
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.spyk
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class AuthRepositoryInstrumentedTest {
    companion object {
        val chain = ChainSelector.BCHMAINNET
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }

    private lateinit var privateKeyMock: ByteArray
    private lateinit var payDestination: PayDestination
    private lateinit var secret: Secret
    private lateinit var authProviderFirebaseSpy: AuthProviderFirebase
    private lateinit var identityRepository: IdentityRepository
    private lateinit var authRepository: AuthRepository
    private lateinit var authRepositorySpy: AuthRepository
    private lateinit var address: String
    private lateinit var httpsProviderFirebaseMock: HttpsProviderFirebase
    private lateinit var restProviderAuthSpy: RestProviderAuth
    private lateinit var challengeMock: String

    @BeforeEach
    fun setUp() = runBlocking {
        mockkStatic(FirebaseAuth::class)
        mockkStatic(FirebaseFunctions::class)
        every { FirebaseAuth.getInstance() } returns mockk(relaxed = true)
        every { FirebaseFunctions.getInstance(Constants.region) } returns mockk(relaxed = true)

        privateKeyMock = ElectionFaker.mockPrivateKey()
        payDestination = Pay2PubKeyHashDestination(chain, UnsecuredSecret(privateKeyMock))
        secret = payDestination.secret ?: throw Exception("Private key missing!")
        authProviderFirebaseSpy = spyk(AuthProviderFirebase)
        httpsProviderFirebaseMock = mockk(relaxed = true)
        restProviderAuthSpy = spyk(RestProviderAuth)
        identityRepository = IdentityRepository.getInstance(payDestination)
        authRepository = AuthRepository.getInstance(
            identityRepository,
            authProviderFirebaseSpy,
            httpsProviderFirebaseMock,
            restProviderAuthSpy
        )
        authRepositorySpy = spyk(authRepository, recordPrivateCalls = true)
        address = payDestination.address.toString()
        challengeMock = "mock_challenge"
    }

    @AfterEach
    fun clear() {
        clearAllMocks()
    }

    @Test
    fun getCurrentUserPkh() {
        val pkhMock = payDestination.address.toString()
        every { authProviderFirebaseSpy.getCurrentUserId() } returns pkhMock
        val currentUserPkh = authRepositorySpy.getCurrentUserId() ?: throw Exception("Cannot get currentUserIc")
        assertEquals(pkhMock, currentUserPkh)
    }
}
