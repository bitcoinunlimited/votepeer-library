# VotePeer Android voting library
[ico-telegram]: votepeer/telegram.svg
[link-telegram]: https://t.me/buip129
[![Chat on Telegram][ico-telegram]][link-telegram]
[![Twitter URL](https://img.shields.io/twitter/url/https/twitter.com/votepeer.svg?style=social&label=Follow%20%40votepeer)](https://twitter.com/votepeer)

[![Android](https://img.shields.io/badge/android-support-green.svg)](https://android.com)
[![MIT license](https://img.shields.io/github/license/Naereen/StrapDown.js.svg)](LICENSE)
[![Website](https://img.shields.io/badge/Website_&_Demo-Online-green.svg)](https://voter.cash/)

[![pipeline status](https://gitlab.com/nerdekollektivet/votepeer-library/badges/master/pipeline.svg)](https://gitlab.com/nerdekollektivet/votepeer-library/-/commits/master)

## Importing co-dependencies
### Update project-level `build.gradle to import the following
```groovy
buildscript {
    ext {
        kotlin_version = "1.5.31"
        navigation_version = "2.3.5"
    }
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:7.0.2'
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"

        // Navigation
        classpath "androidx.navigation:navigation-safe-args-gradle-plugin:$navigation_version"

        // Firebase
        classpath "com.google.gms:google-services:4.3.10"
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven {
            url "https://bitcoinunlimited.gitlab.io/libbitcoincashkotlin/repos/"
        }
        maven {
            url "https://bitcoinunlimited.gitlab.io/votepeer-library/repos/"
        }
    }
}
```

### Update app-level `build.gradle`
#### Apply libraries
```groovy
plugins {
    apply plugin: "com.android.application"
    apply plugin: "kotlin-android"
    apply plugin: "com.dicedmelon.gradle.jacoco-android"
    apply plugin: "kotlin-kapt"
    apply plugin: "kotlinx-serialization"
    apply plugin: 'androidx.navigation.safeargs.kotlin'
    apply plugin: 'project-report'
    apply plugin: 'com.google.firebase.crashlytics'
    apply plugin: 'com.google.firebase.firebase-perf'
}
```

#### Import libraries
```groovy
dependencies {

    def lifecycle_version = '2.4.0'
    def room_version = "2.2.6"
    def fragment_version = '1.2.5'
    def work_version = "2.0.0"

    implementation "info.bitcoinunlimited:libbitcoincash:0.2.3"
    implementation "info.bitcoinunlimited:votepeer:0.3.6"
    implementation fileTree(dir: 'libs', include: ['*.jar'])

    // Kotlin
    implementation(kotlin("stdlib-jdk7"))
    implementation "org.jetbrains.kotlinx:kotlinx-serialization-cbor:1.0.0-RC"
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.1'
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-play-services:1.3.3'

    // Android
    implementation 'androidx.appcompat:appcompat:1.2.0'
    implementation 'androidx.core:core-ktx:1.3.2'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    implementation "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"
    implementation "androidx.navigation:navigation-fragment-ktx:$navigation_version"
    implementation "androidx.navigation:navigation-ui-ktx:$navigation_version"
    implementation "androidx.fragment:fragment-ktx:$fragment_version"
    implementation "androidx.work:work-runtime-ktx:$work_version"
    implementation "androidx.room:room-runtime:$room_version"
    implementation "androidx.room:room-ktx:$room_version"
    kapt "androidx.room:room-compiler:$room_version"

    implementation "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-extensions:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-common-java8:$lifecycle_version"
    kapt "androidx.lifecycle:lifecycle-common-java8:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-reactivestreams-ktx:$lifecycle_version"
    implementation "androidx.paging:paging-runtime-ktx:2.1.2"

    // Firebase
    implementation platform('com.google.firebase:firebase-bom:26.4.0')
    implementation "com.google.firebase:firebase-auth-ktx"
    implementation "com.google.firebase:firebase-core"
    implementation "com.google.firebase:firebase-firestore-ktx"
    implementation "com.google.firebase:firebase-functions-ktx"
    implementation "com.google.firebase:firebase-messaging"

    //noinspection GradleDependency
    implementation "com.google.android.material:material:1.1.0"

    // Retrofit HTTP
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"
    implementation "com.squareup.okhttp3:okhttp:4.7.2"
    implementation "com.google.code.gson:gson:2.8.6"
    implementation "androidx.cardview:cardview:1.0.0"
    implementation "androidx.recyclerview:recyclerview:1.1.0"

    // Firebase
    implementation platform("com.google.firebase:firebase-bom:26.0.0")
    implementation "com.google.firebase:firebase-auth-ktx"
    implementation "com.google.firebase:firebase-core"
    implementation "com.google.firebase:firebase-firestore-ktx"
    implementation "com.google.firebase:firebase-functions-ktx"

    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'
}
```

#### Enable databinding in app-level `build.gradle`
```groovy
android {
    //...
    buildFeatures {
        dataBinding true
    }
}
```

## Importing dependencies from the library
### Add application to `AndroidManifest.xml
```xml
<application
    android:name="info.bitcoinunlimited.votepeerexample.ExampleApplication">
    <!-- ... more stuff here ... -->
</application>
```

### Add VotePeerActivity to `AndroidManifest.xml
```xml
<activity android:name="info.bitcoinunlimited.votepeer.votePeerActivity.VotePeerActivity" />
```

## Initiating the votepeer android library

### (dependency) Load libbitcoincash library in `Application()`
```kotlin
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ExampleApplication : Application() {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }
}
```
### use `WalletService.getVotePeerIntent(...)`to get intent
```kotlin
fun startVotePeer() = GlobalScope.launch(Dispatchers.Main){
    val intent = WalletService.getVotePeerIntent(applicationContext, BuildConfig.APPLICATION_ID, true)
    startActivity(intent)
}
```

## Setup the android app with firebase
Here we have imported the votepeer android library into our app

The library currently requires firebase to e connect at the app-level to work

### Add `app/google-services.json` to .gitignore
```gitignore
*.iml
.gradle
/local.properties
/.idea/caches
/.idea/libraries
/.idea/modules.xml
/.idea/workspace.xml
/.idea/navEditor.xml
/.idea/assetWizardSettings.xml
.DS_Store
/build
/captures
.externalNativeBuild
.cxx
local.properties
/.idea/misc.xml
/.idea/*

app/google-services.json
```

### Setup Firebase with your Android App
Follow the tutorial below to set up a firebase project and retrieve your `google-services.json` file

https://firebase.google.com/docs/android/setup

add `google-service.json` to: `app/google-services.json

// TODO:
keys/ file

### Run app!


# For library developers
**Ktlint**

`./gradlew ktlint`

**Unit tests**

`./gradlew test`

**Instrumented unit tests**

`./gradlew connectedAndroidTest`

**Build**

`./gradlew assembleDebug`

**Build release**

`./gradlew publish --exclude-task testDebugUnitTest -i --exclude-task testReleaseUnitTest -i`

## Android Instrumented testing

### Mocking on Android 8 and lower
Some  mocking features supported for android 9 and up only.

See: [Mockk.io/Android](https://mockk.io/ANDROID.html) mocking library requirements

```kotlin
    ./gradlew connectedAndroidTest
```

Android developer documentation: https://developer.android.com/studio/test/command-line#RunTestsGradle

### Android Instrumented test results
These tests currently requires an emulator, or a device to run. Thus they cannot be displayed in CI/CD

<img src="https://i.imgur.com/l2p7kV4.png">

## Make or Build not working?
1. Clean project
2. Delete .gradle and .idea folder
3. Invalidate Caches/Restart
4. Sync gradle
5. Rebuild project